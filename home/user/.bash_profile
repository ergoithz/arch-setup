#
# ~/.bash_profile
#

export PATH="$HOME/.local/bin:$HOME/.pyenv/bin:$PATH"

[[ $(tty) = /dev/tty1 ]] && startxfce4 && logout
[[ -f ~/.bashrc ]] && . ~/.bashrc
